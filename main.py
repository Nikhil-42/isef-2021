from opt_einsum.paths import optimal
from torch.functional import split
from torch.nn.modules.loss import BCELoss, CrossEntropyLoss
from torch.utils import data
from utils import *
from loader import *
from models import *
from datetime import datetime
import torch.nn as nn
from progressbar import progressbar
from collections import namedtuple
import json

from torch.utils.data import DataLoader
from torch.utils.data import random_split

from torch.utils.tensorboard import SummaryWriter

ExperimentSpec = namedtuple('ExperimentSpec', 'id, model, dataset, ds_kwargs, criterion, num', defaults=[0])

exp_dict = {
    "INTL": ExperimentSpec('INTL', INTLModel, INTLDataset, {'transform': torchvision.transforms.ToTensor()}, nn.CrossEntropyLoss()),
    "INTLSTD": ExperimentSpec('INTLSTD', INTLSTDModel, INTLDataset, {'transform': torchvision.transforms.ToTensor()}, nn.CrossEntropyLoss()),
    "XOR": ExperimentSpec('XOR', XORModel, XORDataset, {}, nn.BCELoss()),
    "XORSTD": ExperimentSpec('XORSTD', XORSTDModel, XORDataset, {}, nn.BCELoss()),
    "WBC": ExperimentSpec('WBC', WBCModel, WBCDataset, {}, nn.BCELoss()), 
    "WBCSTD": ExperimentSpec('WBCSTD', WBCSTDModel, WBCDataset, {}, nn.BCELoss()),
    "FMT": ExperimentSpec('FMT', FMNISTDataset, FMNISTModel, {}, nn.CrossEntropyLoss())
}

def get_exp_num(exp_id):
    f = open('exp_nums.json', 'r')
    exp_nums = json.load(f)
    return exp_nums[exp_id]

def set_exp_num(exp_id, num):
    with open('exp_nums.json', 'r+') as f:
        exp_nums = json.load(f)
        exp_nums[exp_id] = num
        f.truncate(0)
        f.seek(0)
        json.dump(exp_nums, f)

def train(model, train_ds, exp, epochs=100, batch_size=32, criterion=nn.BCELoss(), optimizer=None, device='cpu', writer=None):
    if optimizer == None:
        optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.9)

    train_dl = DataLoader(train_ds, batch_size=batch_size, num_workers=4, shuffle=True)

    model.train()
    model.to(device)

    print("Beginning training.")
    loss = 'nan'

    for epoch in progressbar(range(0, epochs), min_value=0, max_value=epochs):
        running_loss = 0
        for i, (inputs, targets) in enumerate(train_dl):
            inputs = inputs.to(device)
            targets = targets.to(device)

            optimizer.zero_grad()

            y_pred = model(inputs)
            loss = criterion(y_pred, targets)

            loss.backward()
            optimizer.step()

            running_loss += loss.item()
            # import pdb; pdb.set_trace() # uncomment to interact with model while training

            for name, param in model.named_parameters():
                writer.add_histogram("Live/"+name, param)

        writer.add_scalar('Live/Loss', running_loss, epoch)

            

    print("Training complete.")

def valid_binary(y_pred, targets):
    return (y_pred.round() == targets).sum()

def valid_categorical(y_pred, targets):
    _val, inds = torch.max(y_pred, 1)
    return (inds == targets).sum()

def validate(model, test_ds, exp, criterion=nn.BCELoss(), device='cpu', writer=None):
    model.eval()
    model.to(device)

    test_dl = DataLoader(test_ds, batch_size=1, num_workers=4, shuffle=False)
    sum_loss = 0

    with torch.no_grad():
        correct = 0
        print("Beginning evaluation.")
        for i, (inputs, targets) in progressbar(enumerate(test_dl), 0, len(test_dl)):
            inputs = inputs.to(device)
            targets = targets.to(device)

            y_pred = model(inputs)
            sum_loss += criterion(y_pred, targets)
            if type(criterion)==nn.BCELoss:
                correct += valid_binary(y_pred, targets)
            elif type(criterion)==nn.CrossEntropyLoss:
                correct += valid_categorical(y_pred, targets)
        print("Evaluation complete.")
        return correct/len(test_ds), sum_loss/len(test_ds)

if __name__ == "__main__":
    
    ConsoleLogging.start(str_input('logs: '))
    try:
        while True:
            exp = exp_dict[str_input('Which experiment are you running? (INTL, XOR, FMNIST, WBC): ').upper()]
            
            if bool_input('Load model from disk? (y/N): ', default=False):
                path = os.path.join('saved_models', exp.id , str_input('filename: '))
                model = torch.load(path)
            else:
                model = exp.model()

            device = str_input("device (cuda): ")
            device = 'cuda' if len(device) == 0 else device

            writer = SummaryWriter('runs/{}_{}'.format(exp.id, get_exp_num(exp.id)))
            

            if bool_input("Perform additional training? (Y/n): "):
                train_ds = exp.dataset(split='train', **exp.ds_kwargs)

                train_kwargs = {}
                # if bool_input("Resume from checkpoint? (y/N): ", default=False):
                #     train_kwargs['checkpoint'] = torch.load(str_input("path: "))
                # else:
                epochs_str = str_input("epochs (100): ")
                batch_size_str = str_input("batch_size (32): ")
            
                train_kwargs['epochs'] = 100 if len(epochs_str) == 0 else int(epochs_str)
                train_kwargs['batch_size'] = 32 if len(batch_size_str) == 0 else int(batch_size_str)
                train_kwargs['device'] = device
        
                train_kwargs['writer'] = writer
                train_kwargs['criterion'] = exp.criterion

                train(model, train_ds, exp, **train_kwargs)

                strtime = datetime.now().strftime('%S.%M.%H-%d.%m.%Y')
                os.makedirs('saved_models/{}/'.format(exp.id), exist_ok=True)
                torch.save(model, 'saved_models/{}/{}.pt'.format(exp.id, strtime))
            
            if bool_input("Perform evaluation of model? (Y/n): "):
                print('ONLY WORKS FOR BCE and CCE')

                test_ds = exp.dataset(split='test', **exp.ds_kwargs)
                accuracy, loss = validate(model, test_ds, exp, writer=writer, criterion=exp.criterion, device=device)
                print('Accuracy: ', accuracy)
                print('Loss: ', loss)

                if bool_input("Add to TensorBoard? (y/N): ", False):
                    writer.add_graph(model, test_ds[0][0].unsqueeze(0).to(device))
            
            writer.close()
            set_exp_num(exp.id, get_exp_num(exp.id) + 1)
            print("Going back to head.")

    except KeyboardInterrupt:
        ConsoleLogging.stop()
        print("Exiting")

        