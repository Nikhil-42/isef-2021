import torch
import pandas
from PIL import Image
from torch import float32
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import transforms
from torchvision.datasets.folder import ImageFolder
import os

class FMNISTDataset(torchvision.datasets.FashionMNIST):

    def __init__(self, split='train', **kwargs):
        super().__init__('data/fashion_mnist', split=='train', download=True, **kwargs)
        self.classes = ('T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
        'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle Boot')

class INTLDataset(ImageFolder):
    """
        Intel Image Dataset

        x.shape = (150, 150, 3) (PIL) | y.shape = (19)
    """
    
    def __init__(self, split='train', **kwargs):
        assert split=='pred' or split=='test' or split=='train'
        super().__init__(
            root=os.path.join('data/intel_image_classification/seg_' + split),
            **kwargs
        )        

class WBCDataset(Dataset):
    """
        Wisconsin Breast Cancer Dataset
        
        x.shape = (10, 3) | y.shape = (1)
    """


    def __init__(self, split='train'):
        self.data = pandas.read_csv("data/wisconsin-breast-cancer.csv")
        if split=='train':
            self.data = self.data.iloc[:469]
        else:
            self.data = self.data.iloc[469:]
        self.data = self.data.replace(('M', 'B'), (0.0, 1.0))
        self.data = self.data.astype('float32')
        

    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, idx):
        return [torch.tensor(self.data.iloc[idx, 2:32], dtype=float32).reshape(torch.Size([3, 10])).transpose(0,1), torch.tensor(self.data.iloc[idx]['diagnosis'], dtype=float32)]

class XORDataset(Dataset):
    """
        XOR Dataset

        x.shape = (2) | y.shape = (1)
    """

    def __init__(self, split='train'):
        self.length = 1000
        self.xs = torch.rand(self.length, 2).round()
        self.ys = torch.empty(self.length, 1)
        for i, x in enumerate(self.xs):
            self.ys[i] = not (x[0] == x[1])

    
    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        return [self.xs[idx], self.ys[idx]]

if __name__ == '__main__':
    ds = INTLDataset(split='test', transform=transforms.Compose([transforms.transforms.ToTensor()]))
    print(len(ds))
    for e in ds:
        print(e)
        if input() == 'exit':
            break