import math
import torch
import opt_einsum
import torch.nn as nn
from torch.nn import Module

"""
    Completed Modules that can be applied to a dataset.
"""

class FMNISTModel(Module):

    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        VectorLinear((16 * 4, 4), (120, 2))

class INTLModel(Module):

    def __init__(self):
        super().__init__()
        self.elu = nn.ELU()
        self.img_tanh = VectorTanh(1)
        self.vec_tanh = VectorTanh(2)
        self.softmax = nn.Softmax(1)
        self.conv1 = nn.Conv2d(3, 10, 3)
        self.max_pool = nn.MaxPool2d(2)
        self.conv2 = nn.Conv2d(10, 5, 3)
        self.vec1 = VectorLinear((36*36, 5), (100, 2))
        self.vec2 = VectorLinear((100, 2), (6, 1))
    
    def forward(self, x):
        x = self.elu(self.conv1(x))
        x = self.max_pool(x)
        x = self.img_tanh(self.conv2(x))
        x = self.max_pool(x)
        x = x.reshape(len(x), 5, -1).transpose(1,2)
        x = self.vec_tanh(self.vec1(x))
        x = self.softmax(self.vec2(x))
        return x.squeeze(-1)

class INTLSTDModel(Module):

    def __init__(self):
        super().__init__()
        self.elu = nn.ELU()
        self.img_tanh = VectorTanh(1)
        self.tanh = nn.Tanh()
        self.softmax = nn.Softmax(1)
        self.conv1 = nn.Conv2d(3, 10, 3)
        self.max_pool = nn.MaxPool2d(2)
        self.conv2 = nn.Conv2d(10, 5, 3)
        self.vec1 = nn.Linear((36*36*5), (100* 2))
        self.vec2 = nn.Linear((100* 2), (6* 1))
    
    def forward(self, x):
        x = self.elu(self.conv1(x))
        x = self.max_pool(x)
        x = self.img_tanh(self.conv2(x))
        x = self.max_pool(x)
        x = x.reshape(len(x), -1)
        x = self.tanh(self.vec1(x))
        x = self.softmax(self.vec2(x))
        return x

class WBCModel(Module):
    """ A model while runs on the data from the WBCD """
    def __init__(self):
        super().__init__()
        self.vec_tanh = VectorTanh(2)
        self.leaky_relu = nn.LeakyReLU()
        self.sigmoid = nn.Sigmoid()
        self.layer0 = VectorLinear((10, 3), (20, 5))
        self.layer1 = VectorLinear((20, 5), (5, 4))
        self.layer2 = VectorLinear((5, 4), (2, 2))
        self.layer3 = VectorLinear((2, 2), (1, 1))
    
    def forward(self, x):
        x = self.vec_tanh(self.layer0(x))
        x = self.vec_tanh(self.layer1(x))
        x = self.vec_tanh(self.layer2(x))
        x = self.sigmoid(self.layer3(x))
        return x.view(-1)

    def to(self, device):
        super().to(device)
        self.device = device

class WBCSTDModel(Module):
    """ A model while runs on the data from the WBCD """
    def __init__(self):
        super().__init__()
        self.tanh = nn.Tanh()
        self.leaky_relu = nn.LeakyReLU()
        self.sigmoid = nn.Sigmoid()
        self.layer0 = nn.Linear(10 * 3, 20 * 5)
        self.layer1 = nn.Linear(20 * 5, 5 * 4)
        self.layer2 = nn.Linear(5 * 4, 2 * 2)
        self.layer3 = nn.Linear(2 * 2, 1)
    
    def forward(self, x):
        x = x.reshape(-1, 30)
        x = self.tanh(self.layer0(x))
        x = self.tanh(self.layer1(x))
        x = self.tanh(self.layer2(x))
        x = self.sigmoid(self.layer3(x))
        return x.view(-1)

    def to(self, device):
        super().to(device)
        self.device = device

class XORSTDModel(Module):
    """ Tests the Vector Layer """
    def __init__(self):
        super().__init__()
        self.linear1 = nn.Linear(2, 2)
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()
        self.linear2 = nn.Linear(2, 1)

    def forward(self, x):
        x = self.relu(self.linear1(x))
        x = self.sigmoid(self.linear2(x))
        return x

    def to(self, device):
        super().to(device)
        self.device = device
    
class XORModel(Module):
    """ Tests the Linear Vector Layer """
    def __init__(self):
        super().__init__()
        self.linear1 = VectorLinear((2,1), (2, 1))
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()
        self.linear2 = VectorLinear((2,1), (1, 1))

    def forward(self, x):
        x = x.unsqueeze(-1)
        x = self.relu(self.linear1(x))
        x = self.sigmoid(self.linear2(x))
        return x.squeeze(-1)

    def to(self, device):
        super().to(device)
        self.device = device

""" 
    Custom Layers
"""
class VectorLinear(Module):

    """ Implementation of a layer which acts on vector valued activations """
    def __init__(self, shape_in, shape_out):
        super().__init__()
        shape_in, shape_out = torch.Size(shape_in), torch.Size(shape_out)
        self.shape_in, self.shape_out = shape_in, shape_out
        weights = torch.randn(*shape_out, *shape_in)
        self.weights = nn.Parameter(weights)
        bias = torch.randn(shape_out)
        self.bias = nn.Parameter(bias)

        nn.init.xavier_normal_(self.weights,gain=math.sqrt(5)) # weight init Switch to xavier init
        fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.weights)
        bound = 1 / math.sqrt(fan_in)
        nn.init.uniform_(self.bias, -bound, bound)  # bias init
    
    def forward(self, x):
        # print("x.shape: ", x.shape)
        w_x = opt_einsum.contract("nwio,bio->bnw", self.weights, x) # W ⊗ x
        return torch.add(w_x, self.bias) # W ⊗ x + b

"""
    Activation functions
"""
class VectorTanh(Module):
    """ A module which maps N-dimensional vector space to a sphere of N-dimensions
            dim should normally be 2
    """
    def __init__(self, dim):
        self.dim = dim
        super().__init__()

    def forward(self, x, dim=1):
        """ x is a tensor of shape (b, n, e) """
        lengths = torch.sqrt(torch.sum(x*x, self.dim))
        tanh = torch.tanh(lengths)
        x = x * (tanh / lengths).unsqueeze(self.dim)
        x[torch.isnan(x)] = 0
        return x

if __name__ == '__main__':
    pass